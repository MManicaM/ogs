+++
title = "Publications"

[menu.main]
weight = 3

+++

<h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">OpenGeoSys publications overview</h1>
<p class="lg:w-1/2 w-full leading-relaxed text-base">If you find OpenGeoSys useful for your research please cite us!</p>

### Cite the software

```bibtex
@software{ogs:6.4.2,
  author = {Naumov, Dmitry Yu. and Bilke, Lars and Fischer, Thomas and
            Rink, Karsten and Wang, Wenqing and Watanabe, Norihiro and
            Lu, Renchao and Grunwald, Norbert and Zill, Florian and
            Buchwald, Jörg and Huang, Yonghui and Bathmann, Jasper and
            Chen, Chaofan and Chen, Shuang and Meng, Boyan and
            Shao, Haibing and Kern, Dominik and Yoshioka, Keita and
            Garibay Rodriguez, Jaime and Miao, Xingyuan and
            Parisio, Francesco and Silbermann, Christian and Thiedau, Jan and
            Walther, Marc and Kaiser, Sonja and Boog, Johannes and
            Zheng, Tianyuan and Meisel, Tobias and Ning, Zhang},
  doi    = {10.5281/zenodo.6405711},
  month  = {4},
  title  = {OpenGeoSys},
  url    = {https://www.opengeosys.org},
  year   = {2022}
}
```

### Highlighted paper

<div class="bg-brand-50 rounded-lg p-2 mb-4 text-gray-900">
{{< bib "kolditz2012" >}}
</div>

<div class="bg-brand-50 rounded-lg shadow p-2 mb-4 text-gray-900">
{{< bib "bilke2019" >}}
</div>

<div class="bg-brand-50 rounded-lg shadow p-2 mb-4 text-gray-900">
{{< bib "lu2022" >}}
</div>

### More publications
